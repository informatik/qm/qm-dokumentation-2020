# FAQ zu QM-Track 2020
[[_TOC_]]

### Wie funktioniert QM-Track?
Das ist in der [Anleitung](README.md) beschrieben.

### Wer darf Issues erstellen?
Jede*r Studierende und Lehrende am Institut für Informatik darf Issues erstellen, Issues kommentieren und voten.


### Wie lang muss ein Issue sein?
Ein Erfahrungsbericht (Issue) ist nicht in der Länge beschränkt. Er sollte jedoch nur so ausführlich wie nötig sein und klar verständlich. Wenn Sie über mehrere Erfahrungen berichten möchten, dann teilen Sie sie auf mehrere Issues auf.


### Was passiert mit den Issues?
Das Sammeln Ihrer Erfahrungen ist der erste Schritt um unsere Studiengänge zu verbessern. Im Anschluss werden aus den Erfahrungen konkrete Probleme erkannt und einem Team übergeben, in dem Studierende, Wissenschaftliche Mitarbeitende und Professorenschaft vertreten sind. Dieses Team entwickelt in einem zweitägigen Workshop (Qualitätsrunde genannt) Maßnahmen zur Verbesserung.


### Ich habe ein konkretes Problem. Wird es gelöst werden?
Es gibt keine Garantie, dass ein Erfahrungsbericht zu einer Lösung führt. Alle Beteiligten geben sich Mühe, die erkannten Probleme zu lösen. Anhand der Aktivität im QM-Track wird analysiert, was die wichtigsten Probleme sind. Viele zustimmende Kommentare und Votes machen es also wahrscheinlicher, dass intensiv an einer Lösung gearbeitet wird.

Sollte das Problem sehr konkreter Natur sein und zum Beispiel einzelne andere Personen betreffen, dann kann auch die Studienberatung weiterhelfen.
studienberatung@informatik.uni-goettingen.de

### Darf ich Lösungsvorschläge für bekannte Probleme einreichen?

Grundsätzlich ja, aber bitte erst nach der detaillierten Beschreibung des Problems bzw. der negativen Erfahrung.
Je genauer das Problem spezifiziert ist, desto besser kann es gelöst werden.


### Kann ich mein Issue anonym einreichen?
Wenn Ihre Erfahrung sehr persönlicher Natur ist, dann gibt es die Möglichkeit ein confidential Issue zu schreiben, welches nicht öffentlich sichtbar ist und nur von Leuten gelesen werden kann, die einen bestimmten Status im Projekt habe.
Zur Zeit sind das Dr. Henrik Brosenne und die studentischen Hilfskräfte Pascal Drude und Lea von Dömming.

Mehr zu [confidential issues in Gitlab](https://gitlab.gwdg.de/help/user/project/issues/confidential_issues.md).

Wir bitten darum nach Möglichkeit öffentliche Issues zu schreiben, da so eine rege Interaktion zwischen den Beteiligten möglich ist.


### Wer kann die Issues lesen?

Es gibt normale Issues und vertrauliche (confidential) Issues. Normale Issues sind in QM-Track für alle zugänglich, damit sich andere Personen durch Voting oder Kommentare anschließen können. Confidential Issues werden nur von einzelnen Personen gelesen und anonymisiert im weiteren Prozess behandelt.

Siehe [Anleitung](README.md#Erfahrungsbericht-hinterlegen) oder Frage ["Kann ich mein Issue anonym einreichen?"](faq.md#Kann-ich-mein-Issue-anonym-einreichen).


### Wann werden die Ergebnisse bekannt gegeben?
QM-Track ist sowohl Issue Tracker, als auch Dokumentensammlung. Der Bearbeitungsstand der Issues und alle Dokumente des Prozesses werden sichtbar sein. So ist alles an einem Ort gesammelt.  Auch die Arbeitsempfehlungen, die aus den Issues erstellt werden, werden in QM-Track veröffentlicht.

Erste Lösungsansätze werden voraussichtlich einige Tage nach der QM-Runde vorliegen.


### Warum gibt es den Issue-Tracker?
Studiengänge müssen regelmäßig prüfen, ob sie studierbar sind. Diesen Vorgang nennt man Akkreditierung. Die Uni Göttingen hat beschlossen, dass sie auf Systemakkreditierung umstellen. Das heißt, statt wie bisher einzelne Studiengänge zu bewerten, wird durch ein von der Fakultät selbst gewähltes Qualitätsmanagementsystem die Studierbarkeit gesichert. Da sich ein Studiengang regelmäßig durch z.B. Moduländerungen und dem Wechsel von Lehrpersonal verändert, muss der Austausch über Probleme zwischen Studierenden und Lehrenden auch regelmäßig stattfinden. Der Issue-Tracker ist ein Werkzeug des neuen Qualitätsmanagement, um diesen Austauch zu verbessern.

Für besonders Interessierte das Konzept der Systemakkreditierung: https://qmhandbuch.uni-goettingen.de/handbuch/qualitaet-in-studium-und-lehre/systemakkreditierung-1

### Meine Frage wurde nicht beantwortet

Dann schreiben Sie uns!
Lea von Dömming und Pascal Drude
qm-stud@informatik.uni-goettingen.de
