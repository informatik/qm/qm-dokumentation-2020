# Datenschutz

Informationen zur Datenerhebung und Datenverarbeitung.

## Verarbeitungszwecke

QM-Management

## Kategorien

Umfrage

## Weitergabe

Daten werden intern in die QM-Runde weitergegeben.
Externe Weitergabe der Daten erfolgt nicht.

## Speicherdauer

Daten in QM-Track, also insbesondere Issues, werden für die Dauer eines Akkreditierungszyklus gespeichert. Ein Zyklus dauert 6 Jahre.


## Rechte

Bei Vorliegen entsprechender gesetztlicher Vorraussetzungen besteht das Recht
auf Berichtigung oder Löschung der Daten, auf Einschränkung der Verarbeitung,
sowie ein Widerspruchsrecht gegen diese Verarbeitung.

## Beschwerde

Es besteht ein Beschwerderecht bei einer Aufsichtsbehörde (nach Art. 77 DSGVO),
z.B. bei
Die Landesbeauftragte für den Datenschutz Niedersachsen
Prinzenstraße 5
30159 Hannover
https://www.lfd.niedersachsen.de
E-Mail poststelle@lfd.niedersachsen.de
Telefon +49 511 120-4500
Telefax +49 511 120-4599

## Erhebung

Daten werden bei der betroffenen Person erhoben.

## Automatisierte Entscheidungsfindung

Die Daten werden nicht für eine automatisierte Entscheidungsfindung,
einschließlich Profiling, verwendet.