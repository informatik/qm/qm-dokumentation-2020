# QM-Track 2020 Dokumentation

[[_TOC_]]

## Deutsch

Dokumentation des Issue-Tracking-Systems [QM-Track 2020](https://gitlab.gwdg.de/informatik/qm/qm-track-2020)
der Lehreinheit Informatik für die QM-Runde (Qualitätsrunde) 2020 für die Studiengänge

- Angewandte Informatik (B. Sc.)
- Angewandte Informatik (M. Sc.)
- Angewandte Data Science (B. Sc.)
- Informatik (2-Fächer-BA)
- Master of Education (Fach Informatik)


### Issues

Als Issue wird ein Erfahrungsbericht zu einem Sachverhalt erwartet, der als gut oder problematisch angesehen
wird und einen/mehrere Studiengänge betrifft oder QM-Track selbst.

In der QM-Runde 2020 werden alle Issues verarbeitet, die QM-Track selbst oder die Studierbarkeit betreffen.
Das umfasst insbesondere:
- Studienorientierungsangebote (vor der Immatrikulation)
- Betreuung der Studieneingangsphase (nach der Immatrikulation)
- Studienberatung
- Studienverweildauer
- Prüfungsorganisation und Prüfungsgleichheit
- Studentischer Workload
- Anrechnung von Leistungen

#### Issues lesen/erstellen/bewerten

Die Issues der QM-Runde 2020
werden im Projekt [QM-Track 2020](https://gitlab.gwdg.de/informatik/qm/qm-track-2020)
gesammelt, das nur für angemeldete Nutzer zugänglich ist, d.h. um Issues zu lesen und zu bewerten
oder um neue Issues zu erstellen, müssen Sie sich bei [gitlab.gwdg.de](https://gitlab.gwdg.de) anmelden.

Sind sie angemeldet, können Sie alle Issues,
mit Ausnahme der vertraulich (_confidential_) hinterlegten, lesen.
Sie können ein Issue bewerten, indem Sie zustimmen
![up](img/up.jpg "up") oder nicht zustimmen ![down](img/down.jpg "down").

Um ein neues Issue zu erstellen, wählen Sie in der linken Spalte **Issues** (grüner Pfeil)
und dann **New Issue** (blauer Pfeil).

Siehe nachstehenden Screenshot (anklicken zum Maximieren).

<img title="click to open" src="/img/issue-new-arrow.jpg" width="500">

#### Erfahrungsbericht hinterlegen

Geben Sie Ihrem Issue einen aussagekräftigen **Titel** (grüner Pfeil).

**Wichtig**. Wählen Sie bei **Description** das Template `Erfahrungsbericht (deutsch)`.

Haken Sie bei
`Einstufung` und `Betrifft` Zutreffendes an (gelbe Pfeile).

**Hinterlegen Sie Ihren
Erfahrungsbericht unter `Sachverhalt` (blauer Pfeil).**

Falls Sie möchten, dass Ihr Issue vertraulich behandelt wird, markieren Sie es
als *confidential* (roter Pfeil). Die Verfasserin/der Verfasser des Issues ist
ausschließlich für den QM-Beauftragten (Dr. Henrik Brosenne) und die studentischen Hilfskräfte  sichtbar (Pascal Drude, Lea von Dömming).
Das Issue wird im weiteren Verlauf nur pseudonymisiert weitergegeben.
Diese Pseudonymisierung betrifft nur die Verfasserin/den Verfasser, nicht den Inhalt. Achten
Sie darauf im Inhalt eines vertraulichen Issue keine Hinweise auf Verfasserin/Verfasser
zu hinterlassen.

Siehe nachstehenden Screenshot (anklicken zum Öffnen).

<img title="click to open" src="/img/issue-de-arrow.jpg" width="500">

Die Labels werden auf Grundlage der Issuebeschreibung von den Administratoren gesetzt.

#### Hinweise zu Inhalten von Issues

Issues sollten Erfahrungsberichte (User Stories) sein.
D.h. sie sollten das Problem oder die positive Erfahrung möglichst genau beschreiben.
Allgemeine Aussagen wie z.B. "mir gefällt folgende Lehrveranstaltung nicht", sind nicht hilfreich.
Bitte achten Sie auch auf respektvolle Kommunikation ("Netiquette").
Verunglimpfungen oder Beschimpfungen von Personen werden nicht akzeptiert und entsprechende Issues umgehend gelöscht.

Da öffentliche Issues von allen gelesen werden können, achten Sie bitte darauf keine persönlichen Daten in Ihren Erfahrungsberichten einzubauen.

### Hintergrund
QM-Track ist Teil des Qualitätsmanagementsystems des Studiendekanats Informatik. Ziel ist die regelmäßige Prüfung der Qualitätsstandards ([Akkreditierung](https://www.akkreditierungsrat.de/de/akkreditierungssystem/akkreditierungssystem)). Seit der Umstellung von Programm- auf Systemakkreditierung findet dieser Prozess innerhalb der Universität unter Beteiligung aller Stakeholder statt.

Im Zyklus von 6 Jahren werden dabei verschiedene Themenbereiche in mehreren Qualitätsrunden behandelt. Die Qualitätsrunde ist ein zweitägiger Prozess. Auf Basis der Issues und relevanten Datensätzen entwickelt ein Team aus Dozierenden und Studierenden Arbeitsempfehlungen für die zuständigen Gremien (z.B. Studienkommission, Vorstand des Instituts).

Themenbereiche sind
1. Didaktisches Konzept
2. Studierbarkeit
3. Studiengangsbezogene Kooperation
4. Ausstattung
5. Transparenz und Dokumentation
6. Diversität, Geschlechtergerechtigkeit und Chancengleichheit
7. Qualitätssicherung

Die erste Qualitätsrunde dieser Art wird durch die Sammlung von Issues in QM-Track vorbereitet. Sie findet im Herbst 2020 statt.

### Datenschutz

Hinweise zum [Datenschutz](datenschutz.md).

### FAQ
Noch Fragen? Hier gibt es [Antworten](faq.md).

## English

Documentation of the issue tracking system [QM-Track 2020](https://gitlab.gwdg.de/informatik/qm/qm-track-2020) of the teaching unit Computer Science for the QM-Runde (quality round) 2020 for the study programs:

- Applied Computer Science (B. Sc.)
- Applied Computer Science (M. Sc.)
- Applied Data Science (B. Sc.)
- Computer Science and X (2-Subject B. A.)
- Master of Education (Subject Computer Science)


### Issues
An issue is expected to be a report on an issue that is considered good or problematic
and concerns one or more study programs or QM-Track itself.

The QM-round 2020 will process all issues concerning QM-Track itself or the feasibility of the study programs. This includes:
- orientation to choose a degree program
- support in the introductory phase
- study guidance
- length of study
- organization of exams and fairness
- student workload
- transferring credits

#### Read/Create/Evaluate Issues

The issues of the QM Round 2020
are being collected in project [QM-Track 2020](https://gitlab.gwdg.de/informatik/qm/qm-track-2020)
, which is only accessible to registered users. To read and evaluate issues or to create new issues you must log in to [gitlab.gwdg.de](https://gitlab.gwdg.de).

If you are logged in, you can view all issues
with the exception of those deposited as (_confidential_).
You can rate an issue by agreeing
![up](img/up.jpg "up") or disagreeing ![down](img/down.jpg "down").

To create a new issue, select **Issues** (green arrow) in the left column
and then **New Issue** (blue arrow).

See screenshot below (click to open).

<img title="click to open" src="/img/issue-new-arrow.jpg" width="500">

#### Leave a report

Give your issue a meaningful **title** (green arrow).

**Important**. For **Description**, select the template `user story (English)`.

Check the applicable box next to
`Rating` and `Degree programs concerned` (yellow arrows).

**Deposit your
experience report under `Subject matter` (blue arrow).**

If you want your issue to be kept confidential, mark it
as *confidential* (red arrow). The author of the issue is
only visible to the QM representative (Dr. Henrik Brosenne) and the student assistants (Pascal Drude, Lea von Dömming).
The issue will only be passed on pseudonymized in the further process.
This pseudonymization only concerns the author, not the content. Make sure you do not leave any reference to the author in the content of a confidential issue.


See screenshot below (click to open).

<img title="click to open" src="/img/issue-en-arrow.jpg" width="500">

The labels are set by the administrators based on the issue description.

#### Notes on the contents of issues

Issues should be user stories.
I.e. they should describe the problem or positive experience as exactly as possible.
General statements such as "I don't like the following course" are not helpful.
Please also pay attention to respectful communication ("netiquette").
Insults of persons are not accepted and corresponding issues will be deleted immediately.

Since public issues can be read by everyone, please make sure to not include personal information in your testimonials.

### Background
QM-Track is part of the quality management system of the Dean's Office of Computer Science. The aim is to regularly review the study programs ([accreditation](https://www.akkreditierungsrat.de/de/akkreditierungssystem/akkreditierungssystem)). Since the switch from program accreditation to system accreditation, this process takes place within the university with the participation of all stakeholders.

In a cycle of 6 years, various topics are dealt with in several quality rounds. The quality round is a two-day process. Based on the issues and relevant data sets, a team of lecturers and students develops work recommendations for the responsible committees (e.g. Study Commission, Institute Board).

Topics are
1. didactic concept
2. feasibility of the study programs
3. course-related cooperation
4. equipment
5. transparency and documentation
6. diversity, gender justice and equal opportunities
7. quality assurance

The first quality round of this kind is prepared by collecting issues in QM-Track. It will take place in autumn 2020.

### FAQ
Any questions? Here are some [answers](faq.md) (German only).

### Privacy policy

Notes on [data protection](datenschutz.md) (German only).
